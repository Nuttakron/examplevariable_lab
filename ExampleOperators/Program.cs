﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleOperators
{
    class Program
    {

        void examplePreProcess() {
            /*
            int numberPostIncrement = 0;
            int numberPreIncrement = 0;

            int nonPreIncrement = 0;

            System.Console.WriteLine("[Non-Pre] Before increment: {0} after increment {1}",
                                                       nonPreIncrement, nonPreIncrement + 1);

            System.Console.WriteLine("Final Rsult {0} ", nonPreIncrement);

            System.Console.WriteLine("[Pre] Before increment: {0} after increment {1}", 
                                                       numberPreIncrement, ++numberPreIncrement);

            System.Console.WriteLine("Final Rsult {0} ", numberPreIncrement);

            System.Console.ReadLine();
            
            System.Console.WriteLine("[Post] Before increment: {0} after increment {1}",
                                                       numberPostIncrement, numberPostIncrement++);

            System.Console.WriteLine("Final Rsult {0} ", numberPostIncrement);

            int currentNumber = 0;
            int expectNumber1 = ++currentNumber;

            System.Console.WriteLine("expectNumber1 {0} ", expectNumber1);
            System.Console.WriteLine("currentNumber {0} ", currentNumber);

            System.Console.ReadLine();
            */
        }

        static void arithmeticLab() {
            // Pre - process
            int firstInput = int.Parse(Console.ReadLine());
            int secondInput = int.Parse(Console.ReadLine());
            double result = 0;

            // Arimethic operators
            result = firstInput + secondInput;
            System.Console.WriteLine("{0} + {1} = {2}", firstInput, secondInput, result);

            result = firstInput - secondInput;
            System.Console.WriteLine("{0} - {1} = {2}", firstInput, secondInput, result);

            result = firstInput * secondInput;
            System.Console.WriteLine("{0} * {1} = {2}", firstInput, secondInput, result);

            result = (double)firstInput / (double)secondInput;
            System.Console.WriteLine("{0} / {1} = {2}", firstInput, secondInput, result);

            result = firstInput % secondInput;
            System.Console.WriteLine("{0} % {1} = {2}", firstInput, secondInput, result);

            result = firstInput;
            System.Console.WriteLine("++{0} = {1}", firstInput, ++result);

            result = firstInput;
            System.Console.WriteLine("{0}++ = {1}", firstInput, result++);

            System.Console.ReadLine();
        }

        static void Main(string[] args)
        {
            arithmeticLab();
        }
    }
}
