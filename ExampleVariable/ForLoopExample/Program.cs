﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForLoopExample
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.Write("Input length number: ");
            int lengthNumber = int.Parse(System.Console.ReadLine());
            /*
            int currentNumber = 0;
            do
            {
                System.Console.Write(currentNumber);
                currentNumber += 1;
            } while (currentNumber <= lengthNumber);
            */

            for (int currentNumber = 0; currentNumber <= lengthNumber; currentNumber += 1) {
                System.Console.Write(currentNumber);
            }

            System.Console.ReadLine();

        }
    }
}
