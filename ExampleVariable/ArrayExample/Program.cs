﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayExample
{
    class Program
    {
        static void Main(string[] args)
        {
            int luckyNumber1 = 76;
            int luckyNumber2 = 89;
            int luckyNumber3 = 69;

            int[] luckyNumbers = { 76, 89, 69, 90, 100, 900 };

            for (int idx = 0; idx < luckyNumbers.Length; idx++)
            {
                System.Console.WriteLine(luckyNumbers[idx]);
            }

            //////////////////////////

            int[] jackpotNumber = new int[1000];

            for( int idx = 0; idx < 1000; idx++)
            {
                jackpotNumber[idx] = idx;
               
            }

            System.Console.WriteLine(jackpotNumber[jackpotNumber.Length - 1]);

            System.Console.ReadLine();

            string name = "Potato";

            System.Console.WriteLine( name[name.Length - 1] );
            
        }
        

    }
}
