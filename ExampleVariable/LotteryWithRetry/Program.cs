﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotteryWithRetry
{
    class Program
    {
        static void Main(string[] args)
        {
            int guessNumber = 0;
            int retry = 0;
            while (retry < 2) {

               System.Console.Write("Input your lucky number: ");
               guessNumber = int.Parse(System.Console.ReadLine());

                if (guessNumber == 78)
                {
                    System.Console.WriteLine("Congrats, you guess right number (78)");
                    break;
                }
                else {
                    retry += 1;
                }
                

            };


            System.Console.ReadLine();
        }
    }
}
