﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionExample2
{
    class Program
    {
        static double calculateTriangleArea(double baseTriangleValue,
                                            double heightTriangleValue)
        {
            double resultArea = 0.5 * baseTriangleValue * heightTriangleValue;
            return resultArea;
        }
             
        static void Main(string[] args)
        {
            double baseValue = 10;
            double heightValue = 20;
            double triangleArea = 0.5 * baseValue * heightValue;

            System.Console.WriteLine(triangleArea);

            System.Console.WriteLine( calculateTriangleArea(20,30) );

            triangleArea = calculateTriangleArea(40, 50);
            System.Console.WriteLine(triangleArea);

            triangleArea = calculateTriangleArea(90, 100);
            System.Console.WriteLine(triangleArea);

            System.Console.ReadLine();
        }
    }
}
