﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lottery
{
    class Program
    {
        static void Main(string[] args)
        {
            int money = 2000;
            int rewardMoney = 2000;
            int expectNumber = 111;
            int guessNumber = 0;

            do {
                System.Console.WriteLine("Your current money = {0}", money);
                System.Console.Write("Input your number: ");
                guessNumber = int.Parse(System.Console.ReadLine());

                //Condition Check reward money
                if (guessNumber == expectNumber) {
                    System.Console.WriteLine("Congratulation right number ({0})", expectNumber);
                    System.Console.WriteLine("You got reward {0} ", rewardMoney);
                    money += rewardMoney;
                }else {
                    System.Console.WriteLine("You got penalty {0} ", rewardMoney);
                    money -= rewardMoney;
                }

              //Condition Input user
            } while (guessNumber != expectNumber);

            System.Console.Write("Result money = {0}", money);

            System.Console.ReadLine();
        }
    }
}
