﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionCallSimple
{
    class Program
    {
        static void printInfo() {
            System.Console.Write("Input name");
            string name = System.Console.ReadLine();

            System.Console.Write("Input Lastname");
            string lastname = System.Console.ReadLine();

            System.Console.Write("Input Age");
            string age = System.Console.ReadLine();

            System.Console.WriteLine("Name: {0}",name);
            System.Console.WriteLine("Last Name: {0}", lastname);
            System.Console.WriteLine("Age: {0}", age);
        }

        static void Main(string[] args)
        {

            printInfo();


            System.Console.ReadLine();
        }
    }
}
