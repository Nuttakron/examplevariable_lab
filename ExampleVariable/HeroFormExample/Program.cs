﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroFormExample
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.Write("name: ");
            string heroName = System.Console.ReadLine();

            System.Console.Write("age: ");
            string heroAge = System.Console.ReadLine();
            int heroAgeNumber = int.Parse(heroAge);

            System.Console.Write("class: ");
            string heroClass = System.Console.ReadLine();

            System.Console.Write("money: ");
            string heroMoney = System.Console.ReadLine();
            int heroMoneyNumber = int.Parse(heroMoney);

            System.Console.Write("lvl: ");
            string heroLvl = System.Console.ReadLine();
            int heroLvlNumber = int.Parse(heroLvl);


            //Condition 1 Buy new equipment with discount
            bool buyNewEquipWithDiscount = heroMoneyNumber >= 10000 && heroClass == "Merchant";
            System.Console.WriteLine("Can buy new equipment with discount ?");
            System.Console.WriteLine("Result: {0}", buyNewEquipWithDiscount);

            //Condition 2 Equip Sword
            System.Console.WriteLine("Can equip Sword ?");
            System.Console.WriteLine("Result: {0}", heroLvlNumber >=5 && (heroClass=="Swordman" || heroClass=="Merchant"));

            //Condition 3 Defeat first boss
            System.Console.WriteLine("Can defeat first boss ?");
            System.Console.WriteLine("Result: {0}", (heroLvlNumber >= 20 && heroAgeNumber >= 18)  || heroMoneyNumber >= 999999);


            System.Console.ReadLine();

        }
    }
}
