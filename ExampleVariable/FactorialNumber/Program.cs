﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorialNumber
{
    class Program
    {
        static void Main(string[] args)
        {


            System.Console.Write("Factorial number");
            int number = int.Parse(System.Console.ReadLine());
            int factorialTotal = 1;

            while (number >= 1)
            {
                //Show step assume input = 5 => 5 4 3 2 1  
                System.Console.Write("{0} ", number);
                factorialTotal = factorialTotal * number;
                number = number - 1;

            }

            System.Console.WriteLine("Result = {0}", factorialTotal);

            System.Console.ReadLine();

        }
    }
}
