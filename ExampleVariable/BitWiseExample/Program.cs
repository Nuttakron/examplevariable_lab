﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitWiseExample
{
    class Program
    {
        static void Main(string[] args)
        {
            int user_001 = 1;
            int vip_011 = 3;
            int admin_111 = 7;

            int permissionManagement_100 = 4;
            int permissionViewFriend_001 = 1;
            int permissionNewContent_010 = 2;

            int permissionInput = int.Parse(System.Console.ReadLine());

            bool isManagmentAccess = (permissionInput & permissionManagement_100) == permissionManagement_100;
            bool isViewFriend = (permissionInput & permissionViewFriend_001) == permissionViewFriend_001;
            bool isNewContent = (permissionInput & permissionNewContent_010) == permissionNewContent_010;


            System.Console.WriteLine("Management access {0}", isManagmentAccess);
            System.Console.WriteLine("ViewFriend access {0}", isViewFriend);
            System.Console.WriteLine("NewContent access {0}", isNewContent);



            /*
            int permissionInput = int.Parse(System.Console.ReadLine());
            //permission check
            if (permissionInput == admin_111) {
                System.Console.WriteLine("Management access");
            }
            //permission check
            if (permissionInput == admin_111 ||
                permissionInput == vip_011
                )
            {
                System.Console.WriteLine("New Content");
            }
            //permission check
            if (permissionInput == admin_111 ||
                permissionInput == vip_011 ||
                permissionInput == user_001
                )
            {
                System.Console.WriteLine("View friend");
            }
            */
        }
    }
}
