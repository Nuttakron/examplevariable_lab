﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiplyTable_1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.Write("Input number: ");
            int number = int.Parse(System.Console.ReadLine());

            for (int multiplyNumber = 1; multiplyNumber <= 12; multiplyNumber += 1)
            {
                System.Console.WriteLine("{0} x {1} = {2}", number, multiplyNumber, number * multiplyNumber);
            }

            System.Console.ReadLine();
        }
    }
}
