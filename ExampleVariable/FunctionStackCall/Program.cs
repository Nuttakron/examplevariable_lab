﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionStackCall
{
    class Program
    {
        static void printAllInformation() {
            printName();
            printAge();
            printTelephone();
            printHeight();
        }

        static void printName() {
            System.Console.WriteLine("Johnny");
            System.Console.WriteLine("Dave");
        }

        static void printAge() {
            System.Console.WriteLine("Rightnow my age is: 90");
        }

        static void printTelephone() {
            System.Console.WriteLine("My Telephone: 0899999999");
        }

        static void printHeight() {
            int height = 180;
            System.Console.WriteLine("Height {0}", height);
        }

        static string formatNameAndLastName(bool isFormat, string name, string lastname) {

            string formatted = name + " | " + lastname;

            if (isFormat == true)
            {
                formatted = "FirstName: " + name + " \nLastName: " + lastname;
            }

            return formatted;
        }

        static int sum(int number1, int number2){
            int result = number1 + number2;
            return result;
        }

        static void Main(string[] args)
        {
            //printAllInformation();

            System.Console.Write("Input name: ");
            string name = System.Console.ReadLine();

            System.Console.Write("Input lastname: ");
            string lastname = System.Console.ReadLine();

            bool isFormat = true;
            string formatted = formatNameAndLastName(isFormat, name, lastname);

            System.Console.WriteLine(formatted);


            System.Console.ReadLine();
        }
    }
}
