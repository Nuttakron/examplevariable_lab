﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionExample
{

    class Program
    {
        static void printJohnInformation() {
            System.Console.WriteLine("Hello John");
            System.Console.WriteLine("Age = 30");
            System.Console.WriteLine("Height = 171");
            System.Console.WriteLine("Weight = 90");
        }

        static void printInformation() {
            System.Console.WriteLine("Name: {0}", System.Console.ReadLine());
            System.Console.WriteLine("Age: {0}", System.Console.ReadLine());
            System.Console.WriteLine("Telephone: {0}", System.Console.ReadLine());
        }

        static void Main(string[] args)
        {
            printJohnInformation();
            printJohnInformation();
            printJohnInformation();
            printInformation();
            printInformation();
            printInformation();

            System.Console.ReadLine();
        }
    }
}
