﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multilytable_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());

            for (int columnHeader = 0; columnHeader <= number; columnHeader++) {
                System.Console.Write("{0} \t", columnHeader);
            }

            System.Console.WriteLine(" \n ======================================");

            for (int row = 1; row <= number; row++) {
                System.Console.Write("R{0} \t", row);

                for (int column = 1; column <= number; column++)
                {
                        System.Console.Write("{0}\t", row * column);
                }

                System.Console.WriteLine("");
            }

            Console.ReadLine();

        }
    }
}
