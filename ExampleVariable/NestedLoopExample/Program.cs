﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestedLoopExample
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int row = 0; row < 5; row++) {
                System.Console.Write("r{0}",row);
                for (int column = 0; column < 10; column++) {
                    System.Console.Write("c{0}", column);
                }
            }

            System.Console.ReadLine();

        }
    }
}
