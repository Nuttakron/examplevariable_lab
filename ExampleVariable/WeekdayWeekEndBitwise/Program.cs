﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeekdayWeekEndBitwise
{
    class Program
    {
        static void Main(string[] args)
        {
            int sunday_0000001 = 1;
            int monday_0000010 = 2;
            int tuesday_0000100 = 4;
            int wednesday_0001000 = 8;
            int thursday_0010000 = 16;
            int friday_0100000 = 32;
            int saturday_1000000 = 64;

            int weekend_1000001 = saturday_1000000 | sunday_0000001;
            int weekday_0111110 = monday_0000010 
                                 | tuesday_0000100 
                                 | wednesday_0001000 
                                 | thursday_0010000 
                                 | friday_0100000;


        }
    }
}
