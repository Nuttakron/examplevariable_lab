﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleRelationalOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Input1: ");
            string input1 = System.Console.ReadLine();

            System.Console.WriteLine("Input2: ");
            string input2 = System.Console.ReadLine();

            int number1 = int.Parse(input1);
            int number2 = int.Parse(input2);

            bool compareNum1And2 = number1 > number2;
            System.Console.WriteLine("{0} == {1} = {2}", input1, input2, number1 == number2);
            System.Console.WriteLine("{0} != {1} = {2}", input1, input2, number1 != number2);
            System.Console.WriteLine("{0} > {1} = {2}", input1, input2, number1 > number2);
            System.Console.WriteLine("{0} < {1} = {2}", input1, input2, number1 < number2);
            System.Console.WriteLine("{0} >= {1} = {2}", input1, input2, number1 >= number2);
            System.Console.WriteLine("{0} <= {1} = {2}", input1, input2, number1 <= number2);

            System.Console.ReadLine();

        }
    }
}
