﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionConvertToAscii
{
    class Program
    {
        static int convertCharToAscii( char character )
        {
            int result = (int)character;
            return result;
        }

        static void Main(string[] args)
        {
            System.Console.Write("Input char: ");
            char value = System.Console.ReadKey().KeyChar;

            int result = convertCharToAscii(value);

            System.Console.WriteLine("\n{0}", result);

            Console.ReadLine();
        }
    }
}
