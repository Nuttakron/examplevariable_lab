﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeApplication
{
    class Program
    {

        static bool isActive() {
            Console.WriteLine("Do Active");
            return true;
        }
        static bool isComputerActive() {
            return false;
        }

        static void Main(string[] args)
        {
            /*
            int score = 0;
            System.Console.Write("Input student score: ");
            score = int.Parse(Console.ReadLine());

            if (score >= 90)
            {
                System.Console.WriteLine("This student got grade A");
                System.Console.WriteLine("This student got reward");
            }
            else if (score >= 80 && score <= 89)
            {
                System.Console.WriteLine("This student got grade B");
            }
            else if (score >= 70 && score <= 79)
            {
                System.Console.WriteLine("This student got grade C");
            }
            else if (score >= 50 && score <= 69)
            {
                System.Console.WriteLine("This student got grade D");
            }
            else
            {
                System.Console.WriteLine("This student got grade F");
            }

            

            System.Console.ReadLine();
            */
            int score = 0;

            System.Console.Write("Enable Check Score ?");
            bool isEnableAGrade = bool.Parse(Console.ReadLine());
            bool checkScore = isEnableAGrade || (Console.ReadLine() == "SonHeadSchool");

            System.Console.Write("Input student score: ");
            score = int.Parse(Console.ReadLine());
            char grade = 'F';

            if (!checkScore)
            {
                if (score >= 90)
                {
                    grade = 'A';
                }
                else if (score >= 80 && score <= 89)
                {
                    grade = 'B';
                }
                else if (score >= 70 && score <= 79)
                {
                    grade = 'C';
                }
                else if (score >= 50 && score <= 69)
                {
                    grade = 'D';
                }
            }
            else
            {
                grade = 'A';
            }
          

            System.Console.WriteLine("This student got grade {0}", grade);

            if( grade == 'A' )
            {
                System.Console.WriteLine("This student got reward");
            }

            System.Console.ReadLine();
            
        }
    }
}
