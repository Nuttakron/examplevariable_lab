﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroProfile
{
    class Program
    {
        static void Main(string[] args)
        {
            int shield_0011 = 3;
            int ward_0010 = 2;
            int sword_0001 = 1;
            int gun_0100 = 4;

            int hat_0111 = 7;
            int mageHat_0010 = 2;
            int featherHat_0100 = 4;
            int armor_0001 = 1;
            int adventureSuit_1111 = 15;

            int novice_10000 = 16;
            int swordMan_0001 = 1;
            int mage_0010 = 2;
            int archer_0100 = 4;

            System.Console.WriteLine("Job {0}", swordMan_0001);
            System.Console.WriteLine("sword: {0}", (swordMan_0001 & sword_0001) == swordMan_0001);
            System.Console.WriteLine("shield: {0}", (swordMan_0001 & shield_0011) == swordMan_0001);
            System.Console.WriteLine("ward: {0}", (swordMan_0001 & ward_0010) == swordMan_0001);
            System.Console.WriteLine("gun: {0}", (swordMan_0001 & gun_0100) == swordMan_0001);
            System.Console.WriteLine("hat: {0}", (swordMan_0001 & hat_0111) == swordMan_0001);
            System.Console.WriteLine("mageHat: {0}", (swordMan_0001 & mageHat_0010) == swordMan_0001);
            System.Console.WriteLine("featherHat: {0}", (swordMan_0001 & featherHat_0100) == swordMan_0001);
            System.Console.WriteLine("armor: {0}", (swordMan_0001 & armor_0001) == swordMan_0001);
            System.Console.WriteLine("adventureSuit: {0}", (swordMan_0001 & adventureSuit_1111) == swordMan_0001);

            Console.WriteLine("===============================");
            Console.Write("Input Job id: ");
            int inputCompare = int.Parse(Console.ReadLine());

            System.Console.WriteLine("Job {0}", inputCompare);
            System.Console.WriteLine("sword: {0}", (inputCompare & sword_0001) == inputCompare);
            System.Console.WriteLine("shield: {0}", (inputCompare & shield_0011) == inputCompare);
            System.Console.WriteLine("ward: {0}", (inputCompare & ward_0010) == inputCompare);
            System.Console.WriteLine("gun: {0}", (inputCompare & gun_0100) == inputCompare);
            System.Console.WriteLine("hat: {0}", (inputCompare & hat_0111) == inputCompare);
            System.Console.WriteLine("mageHat: {0}", (inputCompare & mageHat_0010) == inputCompare);
            System.Console.WriteLine("featherHat: {0}", (inputCompare & featherHat_0100) == inputCompare);
            System.Console.WriteLine("armor: {0}", (inputCompare & armor_0001) == inputCompare);
            System.Console.WriteLine("adventureSuit: {0}", (inputCompare & adventureSuit_1111) == inputCompare);
            System.Console.ReadLine();

        }
    }
}
